/*
public class Main {
    public static void main(String[] args) {
        String[] dna = {"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
        boolean isMutant = AnalizaMutante.esMutante(dna);
        System.out.println("¿Es mutante?: " + isMutant);
    }
}
*/

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.net.InetSocketAddress;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        // Inicia tu servidor HTTP
        HttpServer server;
        try {
            server = HttpServer.create(new InetSocketAddress(8080), 0);
            server.createContext("/mutante", exchange -> new MutanteHandler().handle(exchange));
            server.setExecutor(null);
            server.start();
            System.out.println("Servidor iniciado en el puerto 8080...");
        } catch (IOException e) {
            System.out.println("Error al iniciar el servidor: " + e.getMessage());
        }
    }
}
