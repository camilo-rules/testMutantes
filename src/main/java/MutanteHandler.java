/*import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;

public class MutanteHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if ("POST".equals(exchange.getRequestMethod())) {
            // Lee la secuencia de ADN desde el cuerpo de la solicitud
            String requestBody = new String(exchange.getRequestBody().readAllBytes());
            String[] dna = requestBody.split(",");
            // Verifica si el humano es mutante
            boolean esMutante = AnalizaMutante.esMutante(dna);
            System.out.println(esMutante);
            // Construye la respuesta
            String response;
            if (esMutante) {
                response = "El humano es mutante";
            } else {
                response = "El humano no es mutante";
            }

            // Envía la respuesta al cliente
            exchange.sendResponseHeaders(200, response.length());
            OutputStream outputStream = exchange.getResponseBody();
            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        } else {
            // Método no permitido
            exchange.sendResponseHeaders(403, 0);
            exchange.close();
        }
    }
}
*/

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;

public class MutanteHandler implements RequestHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if ("POST".equals(exchange.getRequestMethod())) {
            // Lee la secuencia de ADN desde el cuerpo de la solicitud
            String requestBody = new String(exchange.getRequestBody().readAllBytes());

            // Convierte el JSON a un objeto Java usando Gson
            Gson gson = new Gson();
            String[] dna = gson.fromJson(requestBody, String[].class);

            // Verifica si el humano es mutante
            boolean esMutante = AnalizaMutante.esMutante(dna);
            System.out.println(esMutante);

            // Construye la respuesta
            String response;
            int codeJson;
            if (esMutante) {
                response = "El humano es mutante";
                codeJson= 200;

            } else {
                response = "El humano no es mutante";
                codeJson= 403;
            }

            // Convierte la respuesta a JSON
            String jsonResponse = gson.toJson(response);

            // Envía la respuesta al cliente
            exchange.sendResponseHeaders(codeJson, jsonResponse.length());
            OutputStream outputStream = exchange.getResponseBody();
            outputStream.write(jsonResponse.getBytes());
            outputStream.flush();
            outputStream.close();
        } else {
            // Método no permitido
            exchange.sendResponseHeaders(403, 0);
            exchange.close();
        }
    }
}